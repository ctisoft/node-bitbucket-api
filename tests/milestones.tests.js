var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var issue = {title: "New Issue", content: "Issue content"};
var milestone = "First milestone";

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".milestones()", function () {
    var milestoneId = null;

    describe(".create()", function () {
      it('should create a new milestone', function (done) {
        repo.milestones().create(milestone, function (err, result) {
          milestoneId = result.id;
          result.name.should.eql(milestone);
          done(err);
        });
      });
    });

    describe(".getById()", function () {
      it('should get the milestone by the id', function (done) {
        repo.milestones().getById(milestoneId, function (err, result) {
          result.name.should.eql(milestone);
          result.id.should.eql(milestoneId);
          done(err);
        });
      });
    });

    describe(".getAll()", function () {
      it('should get all milestones', function (done) {
        repo.milestones().getAll(function (err, result) {
          Array.isArray(result).should.be.ok;
          done(err);
        });
      });
    });

    describe(".update()", function () {
      it('should update a milestone', function (done) {
        repo.milestones().update(milestoneId, "Updated content", function (err, result) {
          result.name.should.eql("Updated content");
          done(err);
        });
      });
    });

    describe(".remove()", function () {
      it('should remove a given milestone', function (done) {
        repo.milestones().remove(milestoneId, function (err, result) {
          (result === null).should.be.ok;
          done(err);
        });
      });
    });

    after(function(done) {
      repo.milestones().getAll(function (err, milestones) {
        if (milestones.length === 0) { return done(); }
        milestones.forEach(function (m) {
          repo.milestones().remove(m.id, function (err, result) {
            done(err);
          });
        })
      });
    });

  });
});
